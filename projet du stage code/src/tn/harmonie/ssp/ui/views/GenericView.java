package tn.harmonie.ssp.ui.views;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JLabel;

import tn.harmonie.ssp.ui.Controleur;
import tn.harmonie.ssp.util.Factory;

public abstract class GenericView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4391266042736898442L;
	protected Controleur controller;
	protected JLabel errorZone=new JLabel("Error");
	
	public Controleur getController() {
		return controller;
	}
	public  GenericView() //throws HeadlessException
	{
		super();
		
		
		this.controller=Factory.getController();
		initialize();
		activateView();
		abonneToActionListener();
		errorZone.setVisible(false);
		
	}
	
	protected abstract void initialize() ;
	/**
	 * permet de postionner le AL pour tous les compsantes Action
	 */
	protected abstract void abonneToActionListener() ;
	/**
	 * permet de positionner la vue ger�e par le controlleur
	 */
	
	protected void activateView() {
		getController().setCurrentView(this);
		
	}
	public void setController(Controleur controller) {
		this.controller = controller;
	}
	
	/****************************************************************************/
	public void close()
	 {
		 this.dispose();
	 }
	 /********************************************************************************/
	 public void display()
	 {
		 this.setVisible(true);
	 }
	 public void addError(String errorMsg) {
		 errorZone.setText(errorMsg);
		 errorZone.setForeground(Color.RED);
		 errorZone.setVisible(true);
	 }
	 public void descError()
	 {
		 errorZone.setVisible(false);
	 }
	 public  void log(String message) {
		 String origine=getController().getCurrentView().getClass().getName();
			System.out.println(origine+":"+message);
		}
}
