package tn.harmonie.ssp.ui.views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import tn.harmonie.ssp.model.Probleme;
import tn.harmonie.ssp.util.CommandEnum;

public class VueProbleme extends GenericView {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7754611314325709406L;
	
	//Probleme p;
	JLabel l_titre;JTextField text_titre;
	JLabel l_statut;JComboBox<String> text_statut;
	JLabel l_classement;JComboBox<String> text_clasS;
	JLabel l_affect;JComboBox<String> text_affect;
	JLabel l_detect;JTextField text_detect;
	JLabel domaine;JComboBox<String> text_domaine;
	JLabel date;JTextField text_date;
	JButton bp;JButton bp2;
	JLabel l_grid;JTextArea text_area;
	protected void abonneToActionListener()
	{
		bp.setName(CommandEnum.PB_Annul.getName());
		bp2.setName(CommandEnum.PB_Enreg.getName());
		bp.addActionListener(getController());
		bp2.addActionListener(getController());
		
	}
	
public JButton getBp() {
	return bp;
}

public JButton getBp2() {
	return bp2;
}

//public Probleme getP() {
//	return p;
//}
//public void setP(Probleme p) {
//	this.p = p;
//}
public JLabel getL_titre() {
	return l_titre;
}
public void setL_titre(JLabel l_titre) {
	this.l_titre = l_titre;
}
public JTextField getText_titre() {
	return text_titre;
}
public void setText_titre(JTextField text_titre) {
	this.text_titre = text_titre;
}
public JLabel getL_statut() {
	return l_statut;
}
public void setL_statut(JLabel l_statut) {
	this.l_statut = l_statut;
}
public JComboBox<String> getText_statut() {
	return text_statut;
}
public void setText_statut(JComboBox<String> text_statut) {
	this.text_statut = text_statut;
}
public JLabel getL_classement() {
	return l_classement;
}
public void setL_classement(JLabel l_classement) {
	this.l_classement = l_classement;
}

public JComboBox<String> getText_clasS() {
	return text_clasS;
}
public void setText_clasS(JComboBox<String> text_clasS) {
	this.text_clasS = text_clasS;
}
public JLabel getL_affect() {
	return l_affect;
}
public void setL_affect(JLabel l_affect) {
	this.l_affect = l_affect;
}
public JComboBox<String> getText_affect() {
	return text_affect;
}
public void setText_affect(JComboBox<String> text_affect) {
	this.text_affect = text_affect;
}
public JLabel getL_detect() {
	return l_detect;
}
public void setL_detect(JLabel l_detect) {
	this.l_detect = l_detect;
}
public JTextField getText_detect() {
	return text_detect;
}
public void setText_detect(JTextField text_detect) {
	this.text_detect = text_detect;
}
public JLabel getDomaine() {
	return domaine;
}
public void setDomaine(JLabel domaine) {
	this.domaine = domaine;
}
public JComboBox<String> getText_domaine() {
	return text_domaine;
}
public void setText_domaine(JComboBox<String> text_domaine) {
	this.text_domaine = text_domaine;
}
public JLabel getDate() {
	return date;
}
public void setDate(JLabel date) {
	this.date = date;
}
public JTextField getText_date() {
	return text_date;
}
public void setText_date(JTextField text_date) {
	this.text_date = text_date;
}
public JLabel getL_grid() {
	return l_grid;
}
public void setL_grid(JLabel l_grid) {
	this.l_grid = l_grid;
}
public JTextArea getText_area() {
	return text_area;
}
public void setText_area(JTextArea text_area) {
	this.text_area = text_area;
}
public void setBp(JButton bp) {
	this.bp = bp;
}
public void setBp2(JButton bp2) {
	this.bp2 = bp2;
}
@Override
protected void initialize()
{
	l_titre=new JLabel("titre");
	text_titre=new JTextField(15);
	l_statut=new JLabel("staut");
	String [] s2= {"nouveau","en cour","r�solue"};
	text_statut=new JComboBox<String>(s2);
	l_classement=new JLabel("classement");
	String [] s={"1","2","3","4"};
	text_clasS=new JComboBox<String>(s);
	l_affect=new JLabel("affect� �");
	String []s3= {"alpha","delta","gamma"};
	text_affect=new JComboBox<String>(s3);
	l_detect=new JLabel("d�tect� par");
	text_detect=new JTextField(20);
	domaine=new JLabel("domaine");
	String [] s4= {"d1","d2","d3","d4"};
	text_domaine=new JComboBox<String>(s4);
	date=new JLabel("date");
	text_date=new JTextField(20);
	bp=new JButton("annuler");
	bp2=new JButton("enregistrer");
	
	l_grid=new JLabel("description d�taill�e");
	text_area=new JTextArea(20, 20);
	this.setLocation(450, 100);
	this.setSize(500,500);
	JPanel panneau_princ=new JPanel(new BorderLayout());
	JPanel pan1=new JPanel(new FlowLayout());
	  JPanel pan2=new JPanel(new FlowLayout()); pan2.add(bp);pan2.add(bp2);this.add(pan2, BorderLayout.SOUTH);//south
	  JPanel pan3=new JPanel(new FlowLayout());pan3.add(l_titre);pan3.add(text_titre);this.add(pan3, BorderLayout.NORTH);//north
	  JPanel panneau=new JPanel(new FlowLayout());
	  JPanel pane1 = new JPanel(new GridLayout(0,2));
	  
	  pane1.add(l_statut);pane1.add(text_statut);
	  pane1.add(l_classement);pane1.add(text_clasS);
	  pane1.add(domaine);pane1.add(text_domaine);
	  pane1.add(date);pane1.add(text_date);
	  pane1.add(l_detect);pane1.add(text_detect);
	  pane1.add(l_affect);pane1.add(text_affect);
	  panneau.add(pane1);//center
	  
	 JPanel panGri=new JPanel(new GridLayout(0, 2));
	 panGri.add(l_grid);panGri.add(text_area);
	 panneau.add(panGri);//center
	 pan1.add(errorZone);
	 panneau_princ.add(panneau, BorderLayout.CENTER);
	 panneau_princ.add(errorZone,BorderLayout.NORTH);
	 this.add(panneau_princ);
	
}
public Probleme convertTo() {
	Probleme p=new Probleme(getText_titre().getText()
			,getText_statut().getSelectedItem().toString(),
			getText_clasS().getSelectedItem().toString(),
			getText_affect().getSelectedItem().toString(),
			getText_detect().getText(),
			null,getText_domaine().getSelectedItem().toString(),getText_area().getText()
			);
	return p;
}



 
}
