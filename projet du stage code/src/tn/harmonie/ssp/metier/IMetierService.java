package tn.harmonie.ssp.metier;
import java.sql.SQLException;
import java.util.List;

import tn.harmonie.ssp.model.*;

public interface IMetierService 
{
	public void ajouterProbleme(Probleme p) throws SQLException;
	public List<Probleme> getProblemespartitre(String titre) ;
	public void modiferProbleme(Probleme p) throws SQLException;
}
