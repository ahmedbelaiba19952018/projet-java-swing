package tn.harmonie.ssp.metier;

import tn.harmonie.ssp.model.Probleme;
import tn.harmonie.ssp.util.Factory;

import java.sql.SQLException;
import java.util.List;

import tn.harmonie.ssp.dao.*;

public class MetierService implements IMetierService {
private IDAOService dao;
private static MetierService instance;

	private MetierService() {
	super();
	this.dao=Factory.getDaoService();
}

	public static MetierService getInstance() {
		if(instance==null) instance=new MetierService();
		return instance;
	}

	@Override
	public void ajouterProbleme(Probleme p) throws SQLException {
//		DAOService DAO=new DAOService();
		dao.insererProbleme(p);
		
	}

	@Override
	public List<Probleme> getProblemespartitre(String titre) {
		return dao.rechercherprobpartitre(titre);
		
	}

	@Override
	public void modiferProbleme(Probleme p) throws SQLException {
		dao.update(p);
		
	}

}
