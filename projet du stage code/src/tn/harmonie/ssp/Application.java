package tn.harmonie.ssp;

import tn.harmonie.ssp.ui.views.VueAcceuil;
import tn.harmonie.ssp.util.Utils;

public class Application {

	public static void main(String[] args) {
		Utils.log("Démarrage de l'appication ...");
		//VueRecherche vue=new VueRecherche();
		VueAcceuil vue=new VueAcceuil();
		//VueConsulterModifierProbleme vue=new VueConsulterModifierProbleme();
		vue.display();
		
	}
}

