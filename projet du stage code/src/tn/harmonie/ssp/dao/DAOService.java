package tn.harmonie.ssp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;



import tn.harmonie.ssp.dao.SingletonConnection;
import tn.harmonie.ssp.metier.MetierService;
import tn.harmonie.ssp.model.Probleme;

public class DAOService implements IDAOService {

	private static DAOService instance;
	public static DAOService getInstance() {
		if(instance==null) instance=new DAOService();
		return instance;
	}
	@Override
	public void insererProbleme(Probleme p) throws SQLException 
	{
			Connection conn = SingletonConnection.getConnection();
			String requette="insert into probleme(titre,"
					+ "statut,classement,affect�_�,d�tect�_par,date,domaine,description)"
					+ " values(?,?,?,?,?,?,?,?)";
			try(PreparedStatement ps=conn.prepareStatement(requette)) {
//				PreparedStatement ps=conn.prepareStatement(requette);
				ps.setString(1, p.getTitre());
				ps.setString(2, p.getStatut());
				ps.setString(3, p.getClassement());
				ps.setString(4, p.getAffect�_�());
				ps.setString(5, p.getD�tect�_par());
				ps.setDate(6, p.getDate());
				ps.setString(7, p.getDomaine());
				ps.setString(8,p.getDescription_d�taill�());
		
				ps.executeUpdate();
				
				//ps.close();
			} catch (SQLException e) {
				throw e;
				//e.printStackTrace();
			}
		
		}
/*********************************************************************/
	
	public List<Probleme> rechercherprobpartitre(String titre) {
	List<Probleme> problemes=new ArrayList();
	Connection conn=SingletonConnection.getConnection();
	PreparedStatement ps;
	try {
		ps = conn.prepareStatement("select * from probleme where titre like ?");
		ps.setString(1,"%"+titre+"%");
		ResultSet rs=ps.executeQuery();
		while(rs.next())
		{
			Probleme p=new Probleme();
			p.setId_pro(rs.getInt("id_pro"));
			p.setTitre(rs.getString("titre"));
			p.setStatut(rs.getString("statut"));
			p.setClassement(rs.getString("classement"));
			p.setAffect�_�(rs.getString("affect�_�"));
			p.setD�tect�_par(rs.getString("d�tect�_par"));
			p.setDate(rs.getDate("date"));
			p.setDomaine(rs.getString("domaine"));
			p.setDescription_d�taill�(rs.getString("description"));
			problemes.add(p);
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return problemes;
	
}
@Override
public void update(Probleme p) throws SQLException 
{

		Connection conn = SingletonConnection.getConnection();
		//Statement instruction = conn.createStatement();
		String req="UPDATE probleme SET titre='"+"iiii"+ "' WHERE"
				+ " id_pro="+"61";
		try(PreparedStatement ps=conn.prepareStatement(req)) {
//			PreparedStatement ps=conn.prepareStatement(requette);
			/*ps.setString(1, p.getTitre());
			ps.setString(2, p.getStatut());
			ps.setString(3, p.getClassement());
			ps.setString(4, p.getAffect�_�());
			ps.setString(5, p.getD�tect�_par());
			ps.setDate(6, p.getDate());
			ps.setString(7, p.getDomaine());
			ps.setString(8,p.getDescription_d�taill�());
			ps.setInt(9, p.getId_pro());*/
			ps.executeUpdate();
	
		//instruction.executeUpdate(req);
	} catch (SQLException e) {
		e.printStackTrace();
		//throw e;
	}
}
	
	
	

		
		
}
