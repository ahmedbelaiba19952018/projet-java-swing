package tn.harmonie.ssp.model;

import java.sql.Date;

public class Probleme {
	private int id_pro;
	private String titre;
	private String statut ;
	private String classement;
	private String affect�_�;
	private String d�tect�_par;
	private Date date;
	private String domaine;
	private String description_d�taill�;
	public Probleme() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId_pro() {
		return id_pro;
	}
	public void setId_pro(int id_pro) {
		this.id_pro = id_pro;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getStatut() {
		return statut;
	}
	public void setStatut(String statut) {
		this.statut = statut;
	}
	public String getClassement() {
		return classement;
	}
	public void setClassement(String classement) {
		this.classement = classement;
	}
	public String getAffect�_�() {
		return affect�_�;
	}
	public void setAffect�_�(String affect�_�) {
		this.affect�_� = affect�_�;
	}
	public String getD�tect�_par() {
		return d�tect�_par;
	}
	public void setD�tect�_par(String d�tect�_par) {
		this.d�tect�_par = d�tect�_par;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getDomaine() {
		return domaine;
	}
	public void setDomaine(String domaine) {
		this.domaine = domaine;
	}
	public String getDescription_d�taill�() {
		return description_d�taill�;
	}
	public void setDescription_d�taill�(String description_d�taill�) {
		this.description_d�taill� = description_d�taill�;
	}
	public Probleme(String titre, String statut, String classement, String affect�_�, String d�tect�_par, Date date,
			String domaine, String description_d�taill�) {
		super();
		this.titre = titre;
		this.statut = statut;
		this.classement = classement;
		this.affect�_� = affect�_�;
		this.d�tect�_par = d�tect�_par;
		this.date = date;
		this.domaine = domaine;
		this.description_d�taill� = description_d�taill�;
	}
	
	
}
