package tn.harmonie.ssp.util;

import tn.harmonie.ssp.dao.DAOService;
import tn.harmonie.ssp.dao.IDAOService;
import tn.harmonie.ssp.metier.IMetierService;
import tn.harmonie.ssp.metier.MetierService;
import tn.harmonie.ssp.ui.Controleur;

public class Factory {
public static IDAOService getDaoService() {
	return DAOService.getInstance();
}
public static IMetierService getMetierService() {
	return MetierService.getInstance();
}
public static Controleur getController() {
	return Controleur.getInstance();
}
}
