package tn.harmonie.ssp.dao;

import java.sql.SQLException;
import java.util.List;

import tn.harmonie.ssp.model.*;

public interface IDAOService
{
	public void insererProbleme(Probleme p) throws SQLException;
	public List<Probleme>rechercherprobpartitre(String titre) ;
	public void update(Probleme p) throws SQLException ;
}
