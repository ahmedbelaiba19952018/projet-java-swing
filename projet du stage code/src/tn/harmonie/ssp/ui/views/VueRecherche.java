package tn.harmonie.ssp.ui.views;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import tn.harmonie.ssp.util.CommandEnum;
import tn.harmonie.ssp.util.Utils;

public class VueRecherche extends GenericView 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 610453640124268499L;
	//private TableauListener tablList=new TableauListener(this); 

	private JTextField text_rech;
	private JButton bouton;
	private JTable jtable;
	private ModelJtable problememodel;
	private JPanel paneln;
	private JPanel panelsouth;
	private JButton bouton_valider;
	@Override
	protected void initialize() {
		panelsouth=new JPanel(new FlowLayout());
		bouton_valider =new JButton("d�tail probleme");
		text_rech=new JTextField(15);
		bouton=new JButton("rechercher");
		panelsouth.add(bouton_valider);
		this.add(panelsouth,BorderLayout.SOUTH);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocation(450, 100);
		this.setSize(500,500);
		paneln=new JPanel(new FlowLayout());
		paneln.add(text_rech);paneln.add(bouton);
		this.add(paneln, BorderLayout.NORTH);
		problememodel=new ModelJtable();
		jtable=new JTable(problememodel);
		
		JScrollPane jscr=new JScrollPane(jtable);
		this.add(jscr, BorderLayout.CENTER);
		this.setTitle("recherche");
	}
	
	public void setProblememodel(ModelJtable problememodel) {
		this.problememodel = problememodel;
	}

	public void setText_rech(JTextField text_rech) {
		this.text_rech = text_rech;
	}

	public JTextField getText_rech() {
		return text_rech;
	}

	public ModelJtable getProblememodel() {
		return problememodel;
	}
    public JTable getJtable() {
		return jtable;
	}
    @Override
	protected void abonneToActionListener() {
		//jtable.addMouseListener(tablList);
		bouton.setName(CommandEnum.Recherche.getName());
		bouton_valider.setName(CommandEnum.Recherche_Valid.getName());
		bouton.addActionListener(getController());
		bouton_valider.addActionListener(getController());
	}
	/*public void activerBoutonValider()
	{
		bouton_valider.setEnabled(true);
	}*/
    public Vector<String> convetLigneJtable()
    {   Vector<String> row =null;
    	int nbrscolone,numeroligne,i;
    	nbrscolone=this.getJtable().getColumnCount();
    	numeroligne=this.getJtable().getSelectedRow();
    	if(numeroligne!=-1)
    	{
    		row=new Vector<String>();
    		for(i=0;i<nbrscolone;i++)
    	    {
    			row.add((String) this.getProblememodel().getValueAt(numeroligne, i));
    		}
    	}
    	Utils.log("VueRecherche:convetLigneJtable: row="+row);
    	return row;
    }
}

