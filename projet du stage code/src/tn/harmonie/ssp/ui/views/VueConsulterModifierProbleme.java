package tn.harmonie.ssp.ui.views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import tn.harmonie.ssp.model.Probleme;
import tn.harmonie.ssp.util.CommandEnum;

public class VueConsulterModifierProbleme extends GenericView {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Vector<String> vecto;
	private JTabbedPane onglet;
	private JPanel panneau1,panneau2,panneau3;
	private JPanel sous_panneau1;
	//private JLabel l_titre;
	private JLabel valeur_titre;
	private JRadioButton chek_activer;
	private JRadioButton chek_desactiver;
	private ButtonGroup group ;
	
	
	public JRadioButton getChek_activer() {
		return chek_activer;
	}
	public void setChek_activer(JRadioButton chek_activer) {
		this.chek_activer = chek_activer;
	}
	public JRadioButton getChek_desactiver() {
		return chek_desactiver;
	}
	public void setChek_desactiver(JRadioButton chek_desactiver) {
		this.chek_desactiver = chek_desactiver;
	}
	JLabel l_titre;
	JTextField text_titre;
	JLabel l_statut;
	JComboBox<String> text_statut;
	JLabel l_classement;
	JComboBox<String> text_clasS;
	JLabel l_affect;
	JComboBox<String> text_affect;
	JLabel l_detect;
	JTextField text_detect;
	JLabel domaine;
	JComboBox<String> text_domaine;
	JLabel date;
	JTextField text_date;
	JButton bp;
	JButton bp2;
	JLabel l_grid;
	JTextArea text_area;
	JLabel l_Idpro;
	JTextField text_Idpro;
	JButton bouton_enregis_modif;
	JButton bouton_ann_modif;
	JButton quitter;
	
	

	public JTextField getText_titre() {
		return text_titre;
	}
	public JComboBox<String> getText_statut() {
		return text_statut;
	}
	public JComboBox<String> getText_clasS() {
		return text_clasS;
	}
	public JComboBox<String> getText_affect() {
		return text_affect;
	}
	public JTextField getText_detect() {
		return text_detect;
	}
	public JComboBox<String> getText_domaine() {
		return text_domaine;
	}
	public JTextArea getText_area() {
		return text_area;
	}
	public JTextField getText_date() {
		return text_date;
	}
	@Override
	protected void initialize() {
		quitter=new JButton("quitter");
		bouton_enregis_modif=new JButton("enregi modif");
		bouton_ann_modif=new JButton("annul modifi");
		 group = new ButtonGroup();
        chek_activer=new JRadioButton("activer");
        chek_desactiver=new JRadioButton("desactiver");
        group.add(chek_activer);
        group.add(chek_desactiver);
		log(getController().getCurrentView().getName());
		vecto=controller.recuperLigne();
		System.out.println(vecto.toString());
		this.setTitle("consuter/modifier probleme");
		this.setLocation(450, 100);
		this.setSize(500,500);
		onglet=new JTabbedPane();
		panneau1=new JPanel(new BorderLayout());
		panneau2=new JPanel();
        panneau3=new JPanel();
		onglet.addTab("consultation",panneau1);
		onglet.addTab("causes",panneau2);
		onglet.addTab("Actions", panneau3);
		this.getContentPane().add(onglet);
		sous_panneau1=new JPanel(new FlowLayout());
		panneau1.add(sous_panneau1,BorderLayout.NORTH);
		crerePanneauConsul();
		remplirPanneauConsul();
		desactiver_les_champs_de_saisie();
		//sous_panneau1.add(chek_activer);sous_panneau1.add(chek_desactiver);
		}
    private void remplirPanneauConsul()
    {
    	String s=vecto.elementAt(0);
    	text_Idpro.setText(s);
    	s=vecto.elementAt(1);
    	text_titre.setText(s);
    	s=vecto.elementAt(2);
    	text_statut.setSelectedItem(s);
    	s=vecto.elementAt(3);
    	text_clasS.setSelectedItem(s);
    	s=vecto.elementAt(4);
    	text_affect.setSelectedItem(s);
    	s=vecto.elementAt(5);
    	text_detect.setText(s);
    	s=vecto.elementAt(6);
    	text_date.setText(s);
    	s=vecto.elementAt(7);
    	text_domaine.setSelectedItem(s);
    	s=vecto.elementAt(8);
    	text_area.setText(s);
    	
		
		
	}
	private void crerePanneauConsul() {
		l_Idpro=new JLabel("Id_Pro");
		text_Idpro=new JTextField(4);
    	sous_panneau1.add(chek_activer);sous_panneau1.add(chek_desactiver);
    	l_titre=new JLabel("titre");
    	text_titre=new JTextField(15);
    	l_statut=new JLabel("staut");
    	String [] s2= {"nouveau","en cour","r�solue"};
    	text_statut=new JComboBox<String>(s2);
    	l_classement=new JLabel("classement");
    	String [] s={"1","2","3","4"};
    	text_clasS=new JComboBox<String>(s);
    	l_affect=new JLabel("affect� �");
    	String []s3= {"alpha","delta","gamma"};
    	text_affect=new JComboBox<String>(s3);
    	l_detect=new JLabel("d�tect� par");
    	text_detect=new JTextField(20);
    	domaine=new JLabel("domaine");
    	String [] s4= {"d1","d2","d3","d4"};
    	text_domaine=new JComboBox<String>(s4);
    	date=new JLabel("date");
    	text_date=new JTextField(20);
    	//bp=new JButton("annuler");
    	bp2=new JButton("modifier");
    	
    	l_grid=new JLabel("description d�taill�e");
    	text_area=new JTextArea(20, 20);
    	JPanel panneau_princ=new JPanel(new BorderLayout());
    	JPanel pan1=new JPanel(new FlowLayout());
    	JPanel pan2=new JPanel(new FlowLayout());
    	/*pan2.add(bp);*/pan2.add(bp2);pan2.add(bouton_ann_modif);pan2.add(bouton_enregis_modif);
    	pan2.add(quitter);
    	panneau1.add(pan2, BorderLayout.SOUTH);//south
    	//JPanel pan3=new JPanel(new FlowLayout());
    	sous_panneau1.add(l_Idpro);
    	sous_panneau1.add(text_Idpro);
    	sous_panneau1.add(l_titre);
    	sous_panneau1.add(text_titre);
    	//panneau1.add(pan3, BorderLayout.NORTH);//north
    	JPanel panneau=new JPanel(new FlowLayout());
    	JPanel pane1 = new JPanel(new GridLayout(0,2));
    	  
    	  pane1.add(l_statut);pane1.add(text_statut);
    	  pane1.add(l_classement);pane1.add(text_clasS);
    	  pane1.add(domaine);pane1.add(text_domaine);
    	  pane1.add(date);pane1.add(text_date);
    	  pane1.add(l_detect);pane1.add(text_detect);
    	  pane1.add(l_affect);pane1.add(text_affect);
    	  panneau.add(pane1);//center
    	  
    	 JPanel panGri=new JPanel(new GridLayout(0, 2));
    	 panGri.add(l_grid);panGri.add(text_area);
    	 panneau.add(panGri);//center
    	 pan1.add(errorZone);
    	 panneau_princ.add(panneau, BorderLayout.CENTER);
    	 panneau_princ.add(errorZone,BorderLayout.NORTH);
    	 panneau1.add(panneau_princ);
    	 chek_desactiver.setSelected(true);
		
	}
	/*public void decision()
    {
    	if(chek_activer.isSelected())
    	{
    		activer_les_champs_de_saisie();
    	}else if(chek_desactiver.isSelected())
    	{
    		desactiver_les_champs_de_saisie();
    	}
    	
    }*/
	public void desactiver_les_champs_de_saisie() 
	{
		text_affect.setEnabled(false);
		text_area.setEnabled(false);
		text_clasS.setEnabled(false);
		text_date.setEnabled(false);
		text_detect.setEnabled(false);
		text_domaine.setEnabled(false);
		text_Idpro.setEnabled(false);
		text_statut.setEnabled(false);
		text_titre.setEnabled(false);
		
		
	}
	public void activer_les_champs_de_saisie()
	{
		text_affect.setEnabled(true);
		text_area.setEnabled(true);
		text_clasS.setEnabled(true);
		text_date.setEnabled(true);
		text_detect.setEnabled(true);
		text_domaine.setEnabled(true);
		text_statut.setEnabled(true);
		text_titre.setEnabled(true);
	}
	@Override
	protected void abonneToActionListener() 
	{
		bp2.setName(CommandEnum.Modifer_Pro.getName());
		bp2.addActionListener(getController());
		bouton_ann_modif.setName(CommandEnum.Annuler_Modif.getName());
		bouton_ann_modif.addActionListener(getController());
		quitter.setName(CommandEnum.Quiter.getName());
		quitter.addActionListener(getController());
		bouton_enregis_modif.setName(CommandEnum.Enregi_Modif.getName());
		bouton_enregis_modif.addActionListener(getController());
	}
	public Probleme convertTo() {
		Probleme p=new Probleme(getText_titre().getText()
				,getText_statut().getSelectedItem().toString(),
				getText_clasS().getSelectedItem().toString(),
				getText_affect().getSelectedItem().toString(),
				getText_detect().getText(),
				null,getText_domaine().getSelectedItem().toString(),getText_area().getText()
				);
		return p;
	}
	
	

}
