package tn.harmonie.ssp.util;

public enum CommandEnum {
	//Pour les boutons
PB_Enreg("PB_Enreg"),
PB_Annul("PB_Annul"),
Recherche("Recherche"),
Recherche_Valid("Recherche_Valid"),
Modifer_Pro("Modifer_Pro"),
Annuler_Modif("Annuler_Modif"),
Quiter("Quiter"),
Enregi_Modif("Enregi_Modif"),
//Pour les menu
Menu_PB_Add("Menu_PB_Add"),
Menu_PB_Modif("Menu_PB_Modif")
;
private String name;

private CommandEnum(String name) {
	this.name = name;
}

public String getName() {
	return name;
}
	public static CommandEnum parse(String name) {
		for(CommandEnum item:CommandEnum.values())
			if(item.getName().equalsIgnoreCase(name))
				return item;
		return null;
	}
}
