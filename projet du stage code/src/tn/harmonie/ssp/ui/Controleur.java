package tn.harmonie.ssp.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import tn.harmonie.ssp.metier.IMetierService;
import tn.harmonie.ssp.metier.MetierService;
import tn.harmonie.ssp.model.Probleme;
import tn.harmonie.ssp.ui.views.GenericView;
import tn.harmonie.ssp.ui.views.VueAcceuil;
import tn.harmonie.ssp.ui.views.VueConsulterModifierProbleme;
import tn.harmonie.ssp.ui.views.VueProbleme;
import tn.harmonie.ssp.ui.views.VueRecherche;
import tn.harmonie.ssp.util.CommandEnum;
import tn.harmonie.ssp.util.Factory;
import tn.harmonie.ssp.util.Utils;

public class Controleur implements ActionListener {
/*********************************************/
	private GenericView currentView;
	private IMetierService metierService;
	private static Controleur instance;
/**************************************************/
	public Controleur()
{
		super();
		this.metierService=Factory.getMetierService();
}
	/***********************************************/
public void actionPerformed(ActionEvent e) {
	AbstractButton source=(AbstractButton)e.getSource();
	if(source==null)return;
	if((source instanceof JMenuItem) ||(source instanceof JMenu) )
		navigateTo(source);
	else {
		try {
			executeAction(source);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			getCurrentView().addError(e1.getMessage());
		}
	}
}

/************************************************************/
private void navigateTo(AbstractButton source)
{
    Utils.log("navigateTo: start");
	GenericView target=createView(source);
	if(target!=null)	target.display();
	Utils.log("navigateTo: "+target);
	
}
/*********************************************************************/
private void executeAction(AbstractButton source) throws SQLException
{   
	String sourceName=source.getName();
	CommandEnum command=CommandEnum.parse(sourceName);
	switch (command) {
	case PB_Enreg:		
		ajouteProbleme();
		break;
	case PB_Annul:
		getCurrentView().close();
		GenericView vueacceil=new VueAcceuil();
		vueacceil.display();
		break;
	case Recherche:
		rechercherProbleme();
		break;
	case Recherche_Valid:
		   /*VueRecherche vue=(VueRecherche)getCurrentView();
			Vector<String> v=vue.convetLigneJtable();
			System.out.println(v.toString());*/
		Vector<String> v=recuperLigne();
		if(v!=null)
			{
			getCurrentView().close();
			GenericView vueconsulmodifer=new VueConsulterModifierProbleme();
			vueconsulmodifer.display();
			}
		break;
	case Modifer_Pro:
		modifierProbleme();
		break;
	case Annuler_Modif:
		annullerModificatio();
		break;
	case Quiter:
		getCurrentView().close();
		GenericView vueaccei=new VueAcceuil();
		vueaccei.display();
		break;
	case Enregi_Modif:
		update();
	default:
		break;
	}
	
}
private void update() throws SQLException {
	VueConsulterModifierProbleme vue=(VueConsulterModifierProbleme)getCurrentView();
	String s=vue.getText_date().getText();
	Date dateP=Utils.formatDate(s);
	Probleme p=vue.convertTo();
	p.setDate(dateP);
	metierService.modiferProbleme(p);
	JOptionPane.showMessageDialog(null, "Probl�me mis-�-jour avec succ�s",
			"Information", JOptionPane.INFORMATION_MESSAGE);	
}
private void annullerModificatio() {
	VueConsulterModifierProbleme vue=(VueConsulterModifierProbleme)getCurrentView();
	vue.getChek_desactiver().setSelected(true);
	vue.desactiver_les_champs_de_saisie();
	
}
private void modifierProbleme() {
	
	VueConsulterModifierProbleme vue=(VueConsulterModifierProbleme)getCurrentView();
	if(!vue.getChek_activer().isSelected())
	{
		JOptionPane.showMessageDialog(currentView,"choisir activer au lieu "
				+ "de desactiver","message",JOptionPane.INFORMATION_MESSAGE);
	}
	if(vue.getChek_activer().isSelected())
	{
		vue.activer_les_champs_de_saisie();
	}
	
	
}
public Vector<String> recuperLigne()
{
	VueRecherche vue=(VueRecherche)getCurrentView();
	Utils.log("recuperLigne: vue="+vue);
	Vector<String> v=vue.convetLigneJtable();
	return v;
}

/************************************************************/
private void rechercherProbleme() {
	VueRecherche vue=(VueRecherche)getCurrentView();
	String s=vue.getText_rech().getText();
	List<Probleme> pb=metierService.getProblemespartitre(s);
	vue.getProblememodel().load(pb);
	
}
/*****************************************************************/
private void ajouteProbleme() throws SQLException
{
	VueProbleme vue=(VueProbleme)getCurrentView();
	String s=vue.getText_date().getText();
	Date dateP=Utils.formatDate(s);
	Probleme p=vue.convertTo();
	p.setDate(dateP);
	metierService.ajouterProbleme(p);
	JOptionPane.showMessageDialog(null, "Probl�me ajout�e avec succ�s",
			"Information", JOptionPane.INFORMATION_MESSAGE);
	vue.descError();
	vue.close();
	GenericView vueacceuil=new VueAcceuil();
	vueacceuil.display();
}
/***********************************************************************/
private GenericView createView(AbstractButton source) {
	GenericView target=null;
	String sourceName=source.getName();
	Utils.log("createView: sourceName="+sourceName);
	CommandEnum command=CommandEnum.parse(sourceName);
	switch (command) {
	case Menu_PB_Add:
		getCurrentView().close();
		target=new VueProbleme();
		break;
	case Menu_PB_Modif:
		getCurrentView().close();
		target=new VueRecherche();
	default:
		break;
	}
	Utils.log("createView: view="+target);
	return target;
}

/*********************************************************************/
public GenericView getCurrentView() {
	return currentView;
}
/**********************************************************************/

public void setCurrentView(GenericView currentView) {
	this.currentView = currentView;
}
public static Controleur getInstance() {
	if(instance==null) instance=new Controleur();
	return instance;
}

}
