package tn.harmonie.ssp.ui.views;

import java.util.List;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import tn.harmonie.ssp.model.Probleme;


public class ModelJtable extends AbstractTableModel {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String[] nomcolonnes={"idprob","titre","statut","classement","Affect�_�","D�tect�_par",
    		"Date","Domaine","description"};
    private Vector<String[]> rows=new Vector<String[]>();
	public int getColumnCount() {
		return nomcolonnes.length;
	}
    public int getRowCount() {
		return rows.size();
	}
	public String getColumnName(int column)
	{
		return nomcolonnes[column];
	}
	public Object getValueAt(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return rows.get(arg0)[arg1];
	}
	public void load(List<Probleme> prob)
	{
		rows=new Vector<String[]>();
		for(Probleme p:prob)
		{
			rows.add(new String[] {String.valueOf(p.getId_pro()),p.getTitre(),p.getStatut()
					,p.getClassement(),p.getAffect�_�(),p.getD�tect�_par(),String.valueOf(p.getDate()),
					p.getDomaine(),p.getDescription_d�taill�()});
		}
		fireTableChanged(null);
		
	}
}
