package tn.harmonie.ssp.ui.views;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import tn.harmonie.ssp.util.CommandEnum;
/**
 * Constitue l'�cran d'accueil ainsi que le menu de navigation
 * @author pc
 *
 */
public class VueAcceuil extends GenericView {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9212335076527429845L;
	JMenuBar menu_bar;//barre de menu
	JMenu menu1;
	JMenu men2;
	JMenuItem AjouterMenuItem;
	JMenuItem ModifierMenuItem;
	
	protected void initialize() {
		menu_bar=new JMenuBar();//barre de menu
		menu1=new JMenu("Probleme");
		men2=new JMenu("evaluer l'efficacit� des actions");
		AjouterMenuItem=new JMenuItem("ajouter");
		ModifierMenuItem=new JMenuItem("modifier");
		
		this.setLocation(450, 100);
		  this.setSize(500,500);
		  menu1.add(AjouterMenuItem);
		  menu1.add(ModifierMenuItem);
		  menu_bar.add(menu1);menu_bar.add(men2);
		  this.setJMenuBar(menu_bar);
	}
	public JMenuBar getMenu_bar() {
		return menu_bar;
	}

	public void setMenu_bar(JMenuBar menu_bar) {
		this.menu_bar = menu_bar;
	}

	public JMenu getMenu1() {
		return menu1;
	}

	public void setMenu1(JMenu menu1) {
		this.menu1 = menu1;
	}

	public JMenu getMen2() {
		return men2;
	}

	public void setMen2(JMenu men2) {
		this.men2 = men2;
	}

	public JMenuItem getAjouter() {
		return AjouterMenuItem;
	}

	public void setAjouter(JMenuItem ajouter) {
		this.AjouterMenuItem = ajouter;
	}

	public JMenuItem getModifier() {
		return ModifierMenuItem;
	}

	public void setModifier(JMenuItem modifier) {
		this.ModifierMenuItem = modifier;
	}

	
	@Override
	protected void abonneToActionListener() {
		AjouterMenuItem.setName(CommandEnum.Menu_PB_Add.getName());
		AjouterMenuItem.addActionListener(getController());
		ModifierMenuItem.setName(CommandEnum.Menu_PB_Modif.getName());
		ModifierMenuItem.addActionListener(getController());
		
	}
	

}
