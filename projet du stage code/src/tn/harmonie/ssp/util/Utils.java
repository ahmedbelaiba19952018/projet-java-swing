package tn.harmonie.ssp.util;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.JOptionPane;

public class Utils {
	public static Date formatDate(String strDate)
	{
		Date data=null;
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		sdf.setLenient(false);
		java.util.Date parsed=null;
		try {
			parsed=sdf.parse(strDate);
			data=new Date(parsed.getTime());
		} catch (ParseException d) {
			JOptionPane.showMessageDialog(null, "le format du date doit etre "
					+ "sous le forme dd/mm/yyyy", "Erreur", JOptionPane.ERROR_MESSAGE);
		}
		
	    return data;
	}
public static void log(String message) {
	System.out.println(message);
}
}
